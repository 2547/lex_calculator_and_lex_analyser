

%{

	#include <stdio.h>
	#include <string.h> 

	char *appendStr(const char *x, const char *y);

	char *op = "0",i;
	float a, b, temp;
	int line_number = 1;
	int totalOfTokens = 0; 

%}

DIGIT [0-9]+|([0-9]*)"."([0-9]+)
OPERADOR_ARITMETICO "+"|"-"|"*"|"/"

ln \n

%%

{DIGIT} {
	printf("identifier : %s line: %10d \n", yytext, line_number++);
	digi();
}

{OPERADOR_ARITMETICO} {
	printf("operator : %s line: %10d \n", yytext, line_number++);
	detectOperation();
}


{ln} {


   printf("digit : %f line: %10d \n", a, line_number++);
   printf("identifier : %s line: %10d \n", yytext, line_number++);

    printf("Resposta: %f\n", a);

}


%%


detectOperation() {
	op = yytext;

	if(*op=='+') {
		op = "SUM";
	}

	if(*op=='-') {
		op = "SUB";
	}

	if(*op=='*') {
		op = "MUL";
	}

	if(*op=='/') {
		op = "DIV";
	}
}


digi() {
    if(op=="0") {
	    a = atof(yytext);
	} 
	else {

	b=atof(yytext);

	if(op=="SUM") {
		a = a+b;
	}

	if(op=="SUB") {
		a = a-b;
	}

	if(op=="MUL") {
		a = a*b;
	}

	if(op=="DIV") {
		a = a/b;
	}

	
	op="0";
    }
}


char *appendStr(const char *x, const char *y)
{
    size_t nx = strlen(x), ny = strlen(y);
    char *z = malloc(nx + ny + 1);
    if (!z)
        return NULL;
    memcpy(z, x, nx);
    memcpy(z + nx, y, ny + 1);
    return z;
}


main(int argv,char *argc[]) {
	yylex();
}


yywrap() {
	return 1;
}

