%{  
	int n = 0 ;  
	int line_number = 1;
%}

line .*\n  
%% 
"while"|"if"|"else" {
    n++;
    printf("keywords : %s, line: %10d \n", yytext, line_number++);
}
  
"int"|"float" {
    n++;
    printf("keywords : %s line: %10d \n", yytext, line_number++);
}
  
[a-zA-Z_][a-zA-Z0-9_]* {
    n++;
    printf("identifier : %s line: %10d \n", yytext, line_number++);
}
  
"<="|"=="|"="|"++"|"-"|"*"|"+" {
    n++;
    printf("operator : %s line: %10d \n", yytext, line_number++);
}
  
[(){}|, ;]  {
    n++;
    printf("separator : %s line: %10d \n", yytext, line_number++);
}
  
[0-9]*"."[0-9]+ {
    n++;
    printf("float : %s line: %10d \n", yytext, line_number++);
}
  
[0-9]+ {
    n++;
    printf("integer : %s line: %10d \n", yytext, line_number++);
}

.    ;

%%  
   
int main()  {
    
    yyin = fopen("lex_calculator.l", "r");
    yylex(); 
    printf("\n total no. of token = %d\n", n);
}  

yywrap() {
    return 1;
} 

